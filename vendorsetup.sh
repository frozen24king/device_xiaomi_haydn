echo 'Starting to clone stuffs needed to build for Haydn'

# Device common
echo 'Cloning common device tree'
git clone --depth=1 https://gitlab.com/haydn_new/device_xiaomi_sm8350-common -b void-13 device/xiaomi/sm8350-common

# Kernel
echo 'Cloning kernel tree'
git clone --depth=1 https://github.com/frozen24king/android_kernel_xiaomi_sm8350 -b void-13 kernel/xiaomi/sm8350
cd kernel/xiaomi/sm8350
git submodule init && git submodule update KernelSU
cd ../../..

# Vendor
echo 'Cloning vendor tree'
git clone https://github.com/Astridxx/proprietary_vendor_xiaomi_haydn -b lineage-20 vendor/xiaomi/haydn

# Vendor common
echo 'Cloning common vendor tree'
git clone https://github.com/Astridxx/proprietary_vendor_xiaomi_sm8350-common -b lineage-20 vendor/xiaomi/sm8350-common

# Xiaomi
echo 'Cloning hardware xiaomi'
rm -rf hardware/xiaomi && git clone https://github.com/LineageOS/android_hardware_xiaomi -b lineage-20 hardware/xiaomi


# Firmware
echo 'Cloning firmware'
git clone https://gitlab.com/Alucard_Storm/vendor_xiaomi_haydn-firmware -b thirteen vendor/xiaomi/haydn-firmware

# Camera
echo 'Cloning Leica Camera'
git clone https://gitlab.com/Alucard_Storm/haydn-miuicamera -b thirteen-leica vendor/xiaomi/haydn-miuicamera


echo 'delete vendorsetup.sh from device tree once this is done'
